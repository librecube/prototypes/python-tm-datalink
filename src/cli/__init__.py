import os
import xmlrpc.client

import yaml
import click


# load main config file
config_file = os.environ.get("CONFIG_FILE", "config.yml")
if not os.path.exists(config_file):
    raise ValueError(f"The file {config_file} was not found.")
with open(config_file, 'r') as f:
    config = yaml.safe_load(f)

client = xmlrpc.client.ServerProxy(
    f"http://localhost:{config['controller']['port']}", allow_none=True)


@click.group()
def cli():
    pass


@cli.command()
def ping():
    click.echo(client.ping())


@cli.command()
@click.argument("protocol", required=False)
@click.argument("provider", required=False)
def list(protocol, provider):
    result = []
    if protocol is None or protocol.lower() == 'sle':
        if provider:
            result.extend(client.sle_list(provider))
        else:
            result.extend(client.sle_list())
    elif protocol is None or protocol.lower() == 'nis':
        if provider:
            result.extend(client.nis_list(provider))
        else:
            result.extend(client.nis_list())
    click.echo(result)


@cli.command()
@click.argument("protocol")
@click.argument("provider")
@click.argument("channel")
@click.option("-s", "--start")
@click.option("-e", "--end")
def open(protocol, provider, channel, start, end):
    if protocol.lower() == 'sle':
        if start and end:
            client.sle_open(provider, channel, start, end)
        else:
            client.sle_open(provider, channel)
    elif protocol.lower() == 'nis':
        client.nis_open(provider, channel)


@cli.command()
@click.argument("protocol")
@click.argument("provider")
@click.argument("channel")
def close(protocol, provider, channel):
    if protocol.lower() == 'sle':
        client.sle_close(provider, channel)
    elif protocol.lower() == 'nis':
        client.nis_close(provider, channel)
