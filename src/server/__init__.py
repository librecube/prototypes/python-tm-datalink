import time

from return_datalink import ReturnDatalink


def server():
    try:
        server = ReturnDatalink()
        server.startup()
        print("Server started. Press Ctrl-C to stop.")
        while True:
            time.sleep(1)

    except KeyboardInterrupt:
        print("Shutting down...")
        server.shutdown()
