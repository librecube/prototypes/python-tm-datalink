import sle
from functools import partial


class Incoming:

    protocol = "SLE"

    def __init__(self, config, controller, queue):
        self._config = config
        self._controller = controller
        self._queue = queue

        self._clients = {}

        self._controller.register_function(self.list, "sle_list")
        self._controller.register_function(self.open, "sle_open")
        self._controller.register_function(self.close, "sle_close")

    def startup(self):
        pass

    def shutdown(self):
        for client in self._clients.values():
            client.peer_abort()

    def _queue_outgoing(self, topic, data):
        data = data['data'].asOctets()
        self._queue.put(b"%s:%s" % (topic, data))

    def list(self, provider=None):
        channels = []

        if provider and provider not in self._config['providers']:
            return False  # ignore request

        providers = [provider] if provider else self._config['providers']

        for provider in providers:
            config = self._config['providers'].get(provider)
            if config:
                for entry in config['channels']:
                    channels.append((self.protocol, provider, entry))
        return channels

    def open(self, provider, channel, start=None, end=None):

        if start is not None or end is not None:
            raise NotImplementedError  # TODO!

        if (provider, channel) in self._clients:
            return False  # ignore request

        config = self._config['providers'].get(provider)
        if not config:
            return False  # ignore request

        heartbeat = config['heartbeat-interval']
        deadfactor = config['heartbeat-dead-factor']
        version = config['version']
        auth_level = config['auth-level'].lower()
        local_identifier = config['local-id']
        local_password = config['local-password']
        peer_identifier = config['remote-id']
        peer_password = config['remote-password']

        entry = config['channels'].get(channel)
        if not entry:
            return False  # ignore request

        responder_host = entry['host']
        responder_port = entry['port']
        service_instance = entry['service-instance']
        # virtualchannel and spacecraft_id are only
        # defined for rcf channels
        virtualchannel = entry.get('virtualchannel')
        spacecraft_id = entry.get('spacecraft-id')

        kwargs = dict(
            service_instance_identifier=service_instance,
            responder_host=responder_host,
            responder_port=responder_port,
            auth_level=auth_level,
            local_identifier=local_identifier,
            peer_identifier=peer_identifier,
            local_password=local_password,
            peer_password=peer_password,
            heartbeat=heartbeat,
            deadfactor=deadfactor,
            version_number=version,
        )

        if channel.startswith('raf'):
            client = sle.RafServiceUser(**kwargs)
        elif channel.startswith('rcf'):
            client = sle.RcfServiceUser(**kwargs)

        client.bind()
        if not client.wait_for_state(sle.BOUND, 5):
            return False

        if 'onlt' in channel:
            client.frame_indication = partial(self._queue_outgoing, b'onlt')
        elif 'onlc' in channel:
            client.frame_indication = partial(self._queue_outgoing, b'onlc')
        elif 'offl' in channel:
            client.frame_indication = partial(self._queue_outgoing, b'offl')
        else:
            raise ValueError

        if virtualchannel is not None and spacecraft_id is not None:
            client.start([spacecraft_id, 0, virtualchannel])
        else:
            client.start()

        if not client.wait_for_state(sle.ACTIVE, 5):
            client.peer_abort()
            return False

        self._clients[(provider, channel)] = client
        return True

    def close(self, provider, channel):
        if (provider, channel) not in self._clients:
            return False  # ignore request

        # TODO: poll for mode, instead of using time delays
        client = self._clients[(provider, channel)]
        client.stop()

        if not client.wait_for_state(sle.BOUND, 5):
            client.peer_abort()

        client.unbind()
        if not client.wait_for_state(sle.UNBOUND, 5):
            client.peer_abort()

        del self._clients[(provider, channel)]
        return True
