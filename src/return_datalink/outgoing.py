import queue
import threading

import zmq


class Outgoing:

    def __init__(self, config, controller, queue):
        self._config = config
        self._controller = controller
        self._queue = queue

        self._thread = threading.Thread(target=self._poll_queue)
        self._context = zmq.Context()
        self._zmq_socket = self._context.socket(zmq.PUB)
        self._zmq_socket.setsockopt(zmq.LINGER, 0)

    def startup(self):
        self._zmq_socket.bind("tcp://*:{}".format(self._config['port']))
        self._thread.kill = False
        self._thread.start()

    def shutdown(self):
        self._thread.kill = True
        self._thread.join()
        self._zmq_socket.close()
        self._context.term()

    def _poll_queue(self):
        thread = threading.currentThread()
        while not thread.kill:
            try:
                data = self._queue.get(timeout=1)
                self._zmq_socket.send(data)
            except queue.Empty:
                pass
