import os
import queue

import yaml


class ReturnDatalink:

    def __init__(self):
        self._queue = queue.Queue()

        # load main config file
        config_file = os.environ.get("CONFIG_FILE", "config.yml")
        if not os.path.exists(config_file):
            raise ValueError(f"The file {config_file} was not found.")
        with open(config_file, 'r') as f:
            config = yaml.safe_load(f)

        # controller
        from .controller import Controller
        self._controller = Controller(config['controller'])

        # outgoing data handler
        from .outgoing import Outgoing
        self._outgoing = Outgoing(
            config['outgoing'], self._controller, self._queue)

        # incoming data handler
        self._incoming = []
        for entry in config['incoming']:
            if entry['protocol'].lower() == 'sle':
                from .incoming.sle import Incoming
            # elif entry['protocol'].lower() == 'nis':
            #     from .incoming.nis import Incoming
            else:
                raise NotImplementedError

            config_file = entry['config-file']
            if not os.path.exists(config_file):
                raise ValueError(f"The file {config_file} was not found.")
            with open(config_file, 'r') as f:
                incoming_config = yaml.safe_load(f)
            self._incoming.append(
                Incoming(incoming_config, self._controller, self._queue)
            )

    def startup(self):
        self._controller.startup()
        for incoming in self._incoming:
            incoming.startup()
        self._outgoing.startup()

    def shutdown(self):
        for incoming in self._incoming:
            incoming.shutdown()
        self._outgoing.shutdown()
        self._controller.shutdown()
