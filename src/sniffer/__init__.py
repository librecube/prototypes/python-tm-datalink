import os
import argparse

import zmq
import yaml

import slp


parser = argparse.ArgumentParser()
parser.add_argument("--type", default="raw")
parser.add_argument("--fecf", action="store_true")
args = parser.parse_args()

# load main config file
config_file = os.environ.get("CONFIG_FILE", "config.yml")
if not os.path.exists(config_file):
    raise ValueError(f"The file {config_file} was not found.")
with open(config_file, 'r') as f:
    config = yaml.safe_load(f)

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.setsockopt(zmq.SUBSCRIBE, b"")
socket.connect(f"tcp://localhost:{config['outgoing']['port']}")


def sniffer():

    try:
        while True:
            topic, data = socket.recv().split(b':', maxsplit=1)
            print(80*'-')
            if args.type == "raw":
                print(topic, ":")
                print(slp.hexdump(data, length=32))
            elif args.type == "frame":
                try:
                    frame = slp.TelemetryTransferFrame.decode(
                        data, has_fecf=args.fecf)
                    print(topic, ':', frame)
                except slp.DecodingError:
                    print("Could not decode received data, skipping...")

    except KeyboardInterrupt:
        socket.close()
        context.term()
