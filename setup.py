import setuptools

with open("README.md", encoding="utf-8") as f:
    readme = f.read()

setuptools.setup(
    name="return-datalink",
    version="0.0.1",
    description="Return Datalink Microservice",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://xxxx",
    author="LibreCube",
    author_email="info@librecube.org",
    license="MIT",
    packages=setuptools.find_namespace_packages(where="src"),
    package_dir={"": "src"},
    python_requires=">=3.6",
    install_requires=["pyaml", "click", "pyzmq", "sle"],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'server=server:server',
            'cli=cli:cli',
            'sniffer=sniffer:sniffer']
    }
)
